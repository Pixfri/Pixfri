#### Hey there, I'm Pixfri, here's my presentation:
![](https://komarev.com/ghpvc/?username=your-github-username&label=PROFILE+VIEWS&style=for-the-badge&color=brightgreen)

## My stats:  
![Github stats of Pixfri](https://github-readme-stats.vercel.app/api?username=Pixfri&show_icons=true&theme=gruvbox)  
![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=Pixfri&layout=compact&theme=gruvbox)  

## Languages I use:  
[![My Skills](https://skillicons.dev/icons?i=java,c,cs)](https://skillicons.dev)  
## Languages I learn:  
![](https://skillicons.dev/icons?i=cpp)  
## Editors I use:  
![](https://skillicons.dev/icons?i=idea,vscode,visualstudio)
